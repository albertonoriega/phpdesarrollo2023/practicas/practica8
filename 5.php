<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        form div {
            margin-left: 100px;
            margin-bottom: 10px;
            padding: 10px;
        }

        input {
            position: absolute;
            left: 300px;
        }

        button {
            padding: 10px;
            width: 100px;
        }

        .resultados {
            margin-left: 100px;


        }
    </style>
</head>

<body>
    <form action="">
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" id="nombre" name="nombre" required>
        </div>
        <div>
            <label for="apellidos">Apellidos</label>
            <input type="text" id="apellidos" name="apellidos" required>
        </div>
        <div>
            <label for="direccion">Dirección</label>
            <input type="text" id="direccion" name="direccion" required>
        </div>
        <div>
            <label for="cp">Código postal</label>
            <input type="number" id="cp" name="cp" required>
        </div>
        <div>
            <label for="telefono">Teléfono</label>
            <input type="text" id="telefono" name="telefono" max="999999999" required>
        </div>
        <div>
            <label for="correo">Correo</label>
            <input type="email" id="correo" name="correo" required>
        </div>
        <div>
            <button name="enviar">Enviar</button>
        </div>
    </form>

    <?php
    if (isset($_GET["enviar"])) {
        $nombre = $_GET['nombre'];
        $apellidos = $_GET['apellidos'];
        $direccion = $_GET['direccion'];
        $cp = $_GET['cp'];
        $telefono = $_GET['telefono'];
        $correo = $_GET['correo'];

    ?>
        <div class="resultados">
        <?php
        echo "Nombre: $nombre<br>";
        echo "Apellidos: $apellidos<br>";
        echo "Dirección: $direccion<br>";
        echo "Código Postal: $cp<br>";
        echo "Teléfono: $telefono<br>";
        echo "Correo: $correo <br>";
    }
        ?>
        </div>
</body>

</html>